<?php

include 'includes/db.php';

if(isset($_SESSION['loggedin'])){
    header("Location: index.php");
} else {
    if(isset($_POST['submit'])) {
        $username = $_POST['username'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        
        if(!empty($username) && !empty($email) && !empty($password)) {
            
            $username = mysql_escape_string($connection, $username);
            $email = mysql_escape_string($connection, $username);
            $password = mysql_escape_string($connection, $username);
            
            $password = password_hash($password, PASSWORD_BCRYPT, array('cost' => 12));
            
            $query = "INSERT INTO users (username, user_email, user_password) ";
            $query .= "VALUES('{$username}','{$email}', '{$password}')";
            $register_user_query = mysqli_query($connection, $query);
            if(!$register_user_query) {
            die("QUERY FAILED ". mysqli_error($connection) . ' ' . mysqli_error($connection));
            }
            
            $message = "Your registration submission was successful!";
        } else {
            $message = "Fields cannot be empty!";
        }
    } else {
        $message = "";
    }
  }

?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SpigotLeaks // Register</title>

    <link href="./includes/css/bootstrap.min.css" rel="stylesheet">
    <link href="./includes/css/login.css" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
          <p id="profile-name" class="profile-name-card"></p>
          <form class="form-register">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" name="username" id="regUser" class="form-control" placeholder="Username" required autofocus><br />
            <input type="email" name="email" id="regEmail" class="form-control" placeholder="Email address" required><br />
            <input type="password" name="password" id="regPass" class="form-control" placeholder="Password" required>
            <br />      
            <button class="btn btn-lg btn-primary btn-block btn-signin" name="submit" type="submit">Register</button><br />
                <a href="login.php" class="forgot-password">
               Already registered?
          </a>
          </form><!-- /form -->
          <h6 class="text-center"><? echo $message; ?></h6>
      </div><!-- /card-container -->
    </div><!-- /container -->
  </body>
</html>
