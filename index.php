<?php
session_start();

?>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SpigotLeaks // Home</title>

    <link href="./includes/css/bootstrap.min.css" rel="stylesheet">
    <link href="./includes/css/site.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">SpigotLeaks</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
    
	<div class="container">
	  <div class="panel panel-default">
	    <div class="row">
          <div class="col-md-6 col-md-offset-3">
		    <div class="card">		  
			  <div class="card-title">
			    <p><center><h3>Welcome to the site</h3></center></p>
			  </div>
			  
			  <div class="card-action">
			    <p><center>Welcome to SpigotLeaks! I hope you like the site design, its all custom
				  coded. You may be wondering: "why should i use this site?". Well i will updated
				  it with all the latest (and old) leaks, it has a simple interface and no registration
				  required (although optional).<br /><br />Got ideas for the site? Tweet me @TheDiamondYT.</center></p>
			  </div>
		    </div>
          </div>
	    </div>
	  </div>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </body>
</html>